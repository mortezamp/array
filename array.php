<?php
class array{

    $array = new SplFixedArray(20);
    public function insert($index, $value){
        $array[$index] = $value;

    }

    public function getValue($index){
        if($index > 20){
            return -1;
        }else{
            return $array[$index];
        }
    }

    public function sort($array){
        $array = sort($array);
        return $array;
    }

}